const jwt = require("jsonwebtoken");

const secret = "ECommerceAPI";

// Token creation
module.exports.createAccessToken = (user) => {
    const data = {
      id : user._id,
      email : user.email,
      isAdmin : user.isAdmin
    };
    return jwt.sign(data, secret, {});    
  };

// Token Verification
module.exports.verify = (req, res, next) => {
  let token = req.headers.authorization;

  if (typeof token === "undefined") {
    return res.status(401).json({ message: "No token provided" });
  } else {
    token = token.slice(7, token.length);

    jwt.verify(token, secret, function (err, decodedToken) {
      if (err) {
        return res.status(401).json({ message: "Invalid token" });
      } else {
        req.user = decodedToken;

        console.log('Decoded Token:', req.user);

        next();
      }
    });
  }
};

// Admin Verification
module.exports.verifyAdmin = (req, res, next) => {
  console.log('Verifying admin...');
  if (req.user.isAdmin) {
    next();
  } else {
    console.log('Admin verification failed');
    return res.status(403).json({
      auth: 'Failed',
      message: 'Action Forbidden'
    });
  }
};