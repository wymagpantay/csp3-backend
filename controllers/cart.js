const User = require('../models/User');
const Product = require('../models/Product');


exports.getUserCart = async (req, res) => {
  try {
    const userId = req.user.id;

    console.log('User ID:', userId);

    const user = await User.findById(userId).populate('cart.productId');

    // Add a console log statement to log the user's cart
    console.log('User Cart:', user.cart);

    if (!user) {
      return res.status(404).json({ success: false, message: 'User not found' });
    }

    const cartItems = user.cart.map((item) => {
      const product = item.productId;

      if (product) {
        return {
          productId: product._id,
          productName: product.name,
          productDescription: product.description,
          productPrice: product.price,
          quantity: item.quantity,
        };
      }

      console.error(`Product not found for cart item with ID: ${item._id}`);
      return null;
    });

    const filteredCartItems = cartItems.filter((item) => item !== null);

    res.json({ success: true, cart: filteredCartItems });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};

// Add a product to the cart
exports.addToCart = async (req, res) => {
  try {
    if (!req.user) {
      return res.status(401).json({ success: false, message: 'User not authenticated' });
    }

    const userId = req.user.id;

    const { productId, quantity } = req.body;

    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ success: false, message: 'User not found' });
    }

    const existingCartItem = user.cart.find((item) => {
      if (item.productId && item.productId.toString() === productId) {
        return true;
      }
      return false;
    });

    const product = await Product.findById(productId);

    if (!product) {
      return res.status(404).json({ success: false, message: 'Product not found' });
    }

    const currentQuantity = product.quantity;

    if (existingCartItem) {
      console.log('Existing Cart Item:', existingCartItem);
      console.log('Updating Quantity by:', quantity);
      existingCartItem.quantity += quantity;

      if (currentQuantity >= quantity) {
        product.quantity -= quantity;
        await product.save();
      } else {
        return res.status(400).json({ success: false, message: 'Insufficient product quantity' });
      }
    } else {
      console.log('Adding New Product to Cart with Quantity:', quantity);
      user.cart.push({
        productId,
        quantity,
      });

      if (currentQuantity >= quantity) {
        product.quantity -= quantity;
        await product.save();
      } else {
        return res.status(400).json({ success: false, message: 'Insufficient product quantity' });
      }
    }

    await user.save();

    res.json({ success: true, message: 'Product added to cart successfully', cart: user.cart });
  } catch (error) {
    console.error('Error adding product to cart:', error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};

// Remove a product from the cart
exports.removeFromCart = async (req, res) => {
  try {
    const { productId } = req.params;
    const userId = req.user.id; 
 
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ success: false, message: 'User not found' });
    }

    const productIndex = user.cart.findIndex((item) => item.productId.toString() === productId);

    if (productIndex === -1) {
      return res.status(404).json({ success: false, message: 'Product not found in cart' });
    }

    user.cart.splice(productIndex, 1);

    await user.save();

    res.json({ success: true, message: 'Product removed from cart successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};


exports.updateCartItem = async (req, res) => {
  try {
    const { productId } = req.params;
    const { quantity } = req.body;
    const userId = req.user.id; 

    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ success: false, message: 'User not found' });
    }

    const cartItem = user.cart.find((item) => item.productId.toString() === productId);

    if (!cartItem) {
      return res.status(404).json({ success: false, message: 'Product not found in cart' });
    }

    cartItem.quantity = quantity;

    await user.save();

    res.json({ success: true, message: 'Cart item quantity updated successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};