const Product = require("../models/Product");
const User = require("../models/User");
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require("../auth");


//Create a new product
	module.exports.addProduct = (req, res) => {
  const newProduct = new Product({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    quantity: req.body.quantity
  });

  newProduct
    .save()
    .then((product) => {
      res.json({ success: true, product });
    })
    .catch((error) => {
      console.error(error);
      res.status(500).json({ success: false, error: 'Internal Server Error' });
    });
};


//Retrieve all products
	module.exports.getAllProducts = (req, res) => {
		return Product.find({}).then(result => {
			return res.send(result);
		})
		.catch(err => res.send(err))
	};


//Retrieve all active products
	module.exports.getAllActive = (req, res) => {
		return Product.find({ isActive : true }).then(result => {
			return res.send(result);
		})
		.catch(err => res.send(err))
	};


//Retrieving a specific product
	module.exports.getProduct = (req, res) => {
  return Product.findById(req.params.productId)
    .then((product) => {
      if (!product) {
        return res.status(404).json({ success: false, message: 'Product not found' });
      }

      return res.json(product);
    })
    .catch((err) => res.status(500).json({ success: false, message: 'Internal Server Error' }));
};

//Update a product
	module.exports.updateProduct = (req, res) => {
			let updatedProduct = {
				name : req.body.name,
				description	: req.body.description,
				price : req.body.price,
				quantity: req.body.quantity
			};

			return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error) => {

				if (error) {
					return res.send(false);

				} else {				
					return res.send(true);
				}
			})
			.catch(err => res.send(err))
		};


//Archive a product
	module.exports.archiveProduct = (req, res) => {

		let updateActiveField = {
			isActive: false
		}

		return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
		.then((product, error) => {

			//archived successfully
			if(error){
				return res.send(false)

			// failed
			} else {
				return res.send(true)
			}
		})
		.catch(err => res.send(err))

	};


//Activate a product
	module.exports.activateProduct = (req, res) => {

		let updateActiveField = {
			isActive: true
		}

		return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
			.then((product, error) => {

			if(error){
				return res.send(false)

			// failed
			} else {
				return res.send(true)
				}
		})
		.catch(err => res.send(err))

	};


// Search by Product Name
module.exports.searchProductsByName = async (req, res) => {
	try {
	  const { productName } = req.body;
  
	  const products = await Product.find({
		name: { $regex: productName, $options: 'i' }
	  });
  
	  res.json(products);
	} catch (error) {
	  console.error(error);
	  res.status(500).json({ error: 'Internal Server Error' });
	}
};

// Get the list of emails of users who ordered a product
module.exports.getEmailsOfProductBuyers = async (req, res) => {
  const productId = req.params.productId;

  try {
    // Find the product by productId
    const product = await Product.findById(productId);

    if (!product) {
      return res.status(404).json({ message: 'Product not found' });
    }

    const userIds = product.buyers.map(buyer => buyer.userId);

    const productBuyers = await User.find({ _id: { $in: userIds } });

    const emails = productBuyers.map(user => user.email);

    res.status(200).json({ emails });
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while retrieving product buyers' });
  }
};


	exports.searchProductsByPriceRange = async (req, res) => {
		try {
		  const { minPrice, maxPrice } = req.body;
	 
		  const products = await Product.find({
			price: { $gte: minPrice, $lte: maxPrice }
		  });
	  
		  res.status(200).json({ products });
		} catch (error) {
		  res.status(500).json({ error: 'An error occurred while searching for products' });
		}
};
