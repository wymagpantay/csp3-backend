const User = require("../models/User");
const Order = require('../models/Order');
const Product = require("../models/Product");
const bcrypt = require('bcrypt');
const auth = require("../auth");


//Check if the email already exists
	module.exports.checkEmailExists = (reqBody) => {
		return User.find({email : reqBody.email}).then(result => {
			if (result.length > 0) {
				return true;
			} else {
				return false;
			}
		})
		.catch(err => res.send(err))
	};


//User registration
	module.exports.registerUser = (reqBody) => {
  reqBody.cart = [];

  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password, 10),
    cart: reqBody.cart,
  });

  return newUser
    .save()
    .then((user, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    })
    .catch((err) => err);
};


//Login
	module.exports.loginUser = (req, res) => {
  User.findOne({ email: req.body.email })
    .then(result => {
      if (result == null) {
        return res.send(false);
      } else {
        const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
        if (isPasswordCorrect) {
          const { _id, firstName, lastName, isAdmin } = result;

          console.log('User isAdmin:', isAdmin);

          const accessToken = auth.createAccessToken({ _id, firstName, lastName, isAdmin });

          return res.send({ access: accessToken, firstName, lastName, isAdmin });
        } else {
          return res.send(false);
        }
      }
    })
    .catch(err => res.send(err))
};




//Retrieve user details
	module.exports.getProfile = (req, res) => {
		return User.findById(req.user.id)
		.then(result => {
			result.password = "";
			return res.send(result);
		})
		.catch(err => res.send(err))
	};


//Reset Password
	module.exports.resetPassword = async (req, res) => {
		try {

		  const { newPassword } = req.body;
		  const { id } = req.user;
	  
		  const hashedPassword = await bcrypt.hash(newPassword, 10);
	  
		  await User.findByIdAndUpdate(id, { password: hashedPassword });
	  
		  res.status(200).send({ message: 'Password reset successfully' });
		} catch (error) {
		  console.error(error);
		  res.status(500).send({ message: 'Internal server error' });
		}
	};


//Update Profile
	module.exports.updateProfile = async (req, res) => {
		try {
			console.log(req.user);
			console.log(req.body);
		  const userId = req.user.id;

		  const { firstName, lastName, mobileNo } = req.body;

		  const updatedUser = await User.findByIdAndUpdate(
			userId,
			{ firstName, lastName, mobileNo },
			{ new: true }
		  );
		  res.send(updatedUser);
		} catch (error) {
		  console.error(error);
		  res.status(500).send({ message: 'Failed to update profile' });
		}
	  }

//Update Order Status
	module.exports.updateOrderStatus = async (req, res) => {
		try {
		  const { userId, productId, status } = req.body;
	  
		  const user = await User.findById(userId);
		  const order = user.orders.find((order) => order.productId === productId);
	  
		  if (!order) {
			return res.status(404).json({ error: 'Order not found' });
		  }
	  
		  order.status = status;
		  await user.save();
	  
		  res.status(200).json({ message: 'Order status updated successfully' });
		} catch (error) {
		  res.status(500).json({ error: 'An error occurred while updating the order status' });
		}
	  };	


//Update user as admin controller
exports.updateUserAsAdmin = async (req, res) => {
	try {
	  const { userId } = req.body;
	  const user = await User.findById(userId);
  
	  if (!user) {
		return res.status(404).json({ error: 'User not found' });
	  }
  
	  user.isAdmin = true;
	  await user.save();
  
	  res.status(200).json({ message: 'User updated as admin successfully' });
	} catch (error) {
	  res.status(500).json({ error: 'An error occurred while updating the user as admin' });
	}
  };


module.exports.cartCheckout = async (req, res) => {
  try {
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }

    const cartItems = user.cart;
    for (const cartItem of cartItems) {
      const orderItem = {
        productId: cartItem.productId,
        orderedOn: new Date(),
        status: 'Ordered',
      };
      user.orders.push(orderItem);
    }
    user.cart = [];

    await user.save();

    res.status(200).json({ message: 'Cart items moved to orders and cart cleared successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred while moving cart items to orders' });
  }
};


module.exports.getUserOrders = async (req, res) => {
  try {
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }

    const orders = user.orders;
    const populatedOrders = [];

    for (const order of orders) {
      const product = await Product.findById(order.productId);

      if (!product) {
        return res.status(404).json({ error: 'Product not found for order' });
      }

      const populatedOrder = {
        productName: product.name, // Assuming your product model has a 'name' field
        orderedOn: order.orderedOn,
        status: order.status,
      };

      populatedOrders.push(populatedOrder);
    }

    res.status(200).json({ orders: populatedOrders });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred while fetching user orders' });
  }
};











