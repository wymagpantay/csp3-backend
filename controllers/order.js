const Order = require('../models/Order');
const Product = require('../models/Product');
const User = require('../models/User');

// Create a new order
module.exports.createOrder = async (req, res) => {
  try {
    const { productId, quantity } = req.body;

    const product = await Product.findById(productId);

    if (!product) {
      return res.status(404).json({ success: false, message: 'Product not found' });
    }

    const userId = req.user.id;

    if (quantity > product.quantity) {
      return res.status(400).json({ success: false, message: 'Quantity exceeds available stock' });
    }

    const orderPrice = quantity * product.price;

    const order = new Order({
      productId,
      userId,
      productName: product.name,
      description: product.description,
      price: orderPrice,
      quantity,
    });

    await order.save();

    product.quantity -= quantity;
    await product.save();

    const user = await User.findById(userId);
    user.orders.push(order);
    await user.save();

    res.json({ success: true, message: 'Ordered successfully' });
  } catch (error) {
    console.error('Error in createOrder:', error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};


// Fetch all orders
module.exports.getAllOrders = async (req, res) => {
  try {
    const orders = await Order.find();
    res.json({ success: true, orders });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};

// Fetch a specific order by ID
module.exports.getOrderById = async (req, res) => {
  try {
    const orderId = req.params.orderId;
    const order = await Order.findById(orderId);

    if (!order) {
      return res.status(404).json({ success: false, message: 'Order not found' });
    }

    res.json({ success: true, order });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};

// Update a specific order by ID
module.exports.updateOrder = async (req, res) => {
  try {
    const orderId = req.params.orderId;
    const { status } = req.body;

    const order = await Order.findById(orderId);

    if (!order) {
      return res.status(404).json({ success: false, message: 'Order not found' });
    }

    order.status = status;
    await order.save();

    res.json({ success: true, message: 'Order updated successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};

// Delete a specific order by ID
module.exports.deleteOrder = async (req, res) => {
  try {
    const orderId = req.params.orderId;
    const order = await Order.findByIdAndRemove(orderId);

    if (!order) {
      return res.status(404).json({ success: false, message: 'Order not found' });
    }

    res.json({ success: true, message: 'Order deleted successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};