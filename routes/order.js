const express = require('express');
const router = express.Router();
const orderController = require('../controllers/order');
const auth = require("../auth") 

const {verify, verifyAdmin} = auth; 

// Create a new order
router.post('/', verify, orderController.createOrder);

// Get all orders
router.get('/', verify, orderController.getAllOrders);

// Get a specific order by ID
router.get('/:orderId', verify, orderController.getOrderById);

// Update a specific order by ID
router.put('/:orderId', verify, orderController.updateOrder);

// Delete a specific order by ID
router.delete('/:orderId', verify, orderController.deleteOrder);

module.exports = router;