const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cart');
const auth = require("../auth") 

const {verify, verifyAdmin} = auth;

// Fetch user's cart data
router.get('/', verify, cartController.getUserCart);

// Add a product to the cart
router.post('/add', verify, cartController.addToCart);

// Remove a product from the cart
router.delete('/remove/:productId', verify, cartController.removeFromCart);

// Update cart item quantity
router.put('/update/:productId', verify, cartController.updateCartItem);

module.exports = router;