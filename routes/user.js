const express = require('express');
const userController = require("../controllers/user");
const auth = require("../auth") 

const {verify, verifyAdmin} = auth;

const router = express.Router();

//Prevent Duplicate
	router.post("/checkEmail", (req, res) => {
		userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
	})

//User Registration
	router.post("/register", (req, res) => {
		userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
	});

//Login
	router.post("/login", userController.loginUser);

//User Details
	router.get("/details", verify, userController.getProfile);

//Reset Password
	router.put('/reset-password', verify, userController.resetPassword);

//Update Profile	
	router.put('/profile', verify, userController.updateProfile);

//Update order status
	router.put('/orderStatusUpdate', userController.updateOrderStatus);	

//Update Admin Status
	router.put('/updateAdmin', verify, verifyAdmin, userController.updateUserAsAdmin);

//Checkout cart
	router.post('/checkout-cart', verify, userController.cartCheckout);

// Get user orders
router.get('/orders', verify, userController.getUserOrders);

module.exports = router; 
















