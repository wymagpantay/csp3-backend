const express = require('express');
const productController = require("../controllers/product");
const orderController = require('../controllers/order');
const auth = require("../auth") 

const {verify, verifyAdmin} = auth;

const router = express.Router();

//Create a Product
	router.post("/", verify, verifyAdmin, productController.addProduct);

//Retrive all Products
	router.get("/all", productController.getAllProducts);

//Retrieve all the ACTIVE products for all users
	router.get("/", productController.getAllActive);

//Search Product by Name
	router.post('/searchByName', productController.searchProductsByName);	

//Search Products By Price Range
	router.post('/searchByPrice', productController.searchProductsByPriceRange);

//Retrieve a specific product
	router.get("/:productId", productController.getProduct);

//Get the emails of users who ordered a product
	router.get('/:productId/buyers', productController.getEmailsOfProductBuyers);

//Archive a product (Admin)
	router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

//Activate a product (Admin)
	router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);

//Update a Product (Admin)
	router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

module.exports = router;

