//Dependencies and Modules
	const express = require("express");
	const mongoose = require("mongoose");
	const cors = require("cors");
	const userRoutes = require("./routes/user");
	const productRoutes = require("./routes/product");
	const orderRoutes = require("./routes/order");
	const cartRoutes = require("./routes/cart");


//Environment Setup
	const port = 4000;

//Server Setup
	const app = express();
	
	app.use(cors())
	app.use(express.json());
	app.use(express.urlencoded({ extended: true }));


//[SECTION] Database Connection 
	mongoose.connect("mongodb+srv://wymagpantay:lQfFYx3k1wda36gB@batch-297.h8m027r.mongodb.net/Capstone3?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

	let db = mongoose.connection;
	db.on('error', console.error.bind(console,'Connection error'));
	db.once('open', () => console.log('Connected to MongoDB Atlas.'));

//Backend Routes 

	app.use("/users", userRoutes);
	app.use("/products", productRoutes);
	app.use("/orders", orderRoutes);
	app.use("/carts", cartRoutes);

	app.use((err, req, res, next) => {
	  console.error(err);
	  res.status(500).json({ success: false, message: 'Internal Server Error' });
	});



//Server Gateway Response
	if(require.main === module) {
		app.listen( process.env.PORT || port, () => {
			console.log(`API is now online on port ${ process.env.PORT || port }`)
		});
	}


module.exports = {app,mongoose};
